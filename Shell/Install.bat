set settings_replace_folder=product\phone\build\default\outputs\default\phone-default-signed.hap

cd ..

set hdc=hdc.exe

%hdc% shell "mount -o rw,remount /"
%hdc% shell "rm -rf /system/app/com.ohos.settings/Settings.hap"

%hdc% file send %settings_replace_folder% /system/app/com.ohos.settings/Settings.hap

%hdc% shell "rm -rf /data/*"
%hdc% shell "reboot"